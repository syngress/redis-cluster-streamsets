package com.spring.rediscluster.service;

import com.spring.rediscluster.model.entity.flight.Flight;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;

public interface FlightService {
    Flight createFlight(Flight flight, String authorization, String user_id);
    Flight getFlightById(ObjectId id, String authorization, String user_id);
    Page<Flight> getAllFlights(int page, int size, String sort, String direction, String authorization, String user_id);
    Flight modifyFlightById(ObjectId id, Flight flight, String authorization, String user_id);
    Flight deleteFlight(ObjectId id, String authorization, String user_id);

}
