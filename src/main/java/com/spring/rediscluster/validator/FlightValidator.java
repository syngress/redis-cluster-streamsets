package com.spring.rediscluster.validator;

import static com.spring.rediscluster.exception.ExceptionKey.notFound;
import static com.spring.rediscluster.exception.ExceptionKey.unauthorized;
import static com.spring.rediscluster.validator.Validate.isTrue;

public class FlightValidator {
    public void flightNotExist(Boolean flightObject) {
        isTrue(flightObject, notFound, "Flight not found");
    }

}
