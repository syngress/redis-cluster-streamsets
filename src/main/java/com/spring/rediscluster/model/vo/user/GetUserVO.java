package com.spring.rediscluster.model.vo.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.mongodb.core.index.Indexed;

public class GetUserVO {
    @JsonProperty("id")
    @ApiModelProperty(required = true, value = "ObjectID")
    public String id;

    @JsonProperty("username")
    @ApiModelProperty(required = true, value = "Username")
    public String username;

    @JsonProperty("email")
    @Indexed(unique = true)
    @ApiModelProperty(required = true, value = "Email Address")
    public String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY, value = "password")
    @ApiModelProperty(required = true, value = "User Password")
    public String password;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY, value = "salt")
    @ApiModelProperty(required = true, value = "SaltPassword")
    public String salt;

}
