# SpringBoot Redis Enterprise-Class Cluster with StreamSets
![N|Java](https://blog-en.openalfa.com/iconos/logos/java.png)![N|Kafka](https://www.svgimages.com/svg-image/s9/apache-kafka-256x256.png)![RedisIMG](https://syngress.pl/images/redis-cluster.png)


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

Things you may want to cover for local development:

- Java 13
- SpringBoot version 2.2.2
- Apache Maven 3.6.3
- SpringKafka 2.4.0
- Avro 1.9.1
- KafkaAvroSerializer 5.3.0  
- Minimum of RAM 6GB for Redis  
- Elasticsearch 7.6.1  
- Kibana 7.6.1  
- Streamsets 3.11.0  
- MongoDB Database 4.2  
  
Add this to your ```/etc/hosts```  and reboot your workstation  
  
```code
127.0.0.1       kafka
```  
   
# Preparation of Redis Cluster 

As you can see, all redis nodes are launched from the container level.  
It means that part of the work will be fully automated.  
However you need to understand few lines of code related to docker image construction:

- ```cap_add: ['sys_resource']```  
add linux sys resource capabilities to set [proper privileges](http://man7.org/linux/man-pages/man7/capabilities.7.html)  
- ```8443:8443```  
allow to access menagement web user interface  
- ```9443:9443```  
allow to access REST API via HTTPS  
- ```12000:12000```  
that port will be use for database endpoint on this (master) node

# Description of functionalities  

Redis is an open source (BSD licensed), in-memory data structure store, used as a database, cache or message broker.  
For the purposes of our tests, we are run multi-node Redis Cluster with multiple Redis containers deployed to a **single host** machine.  
You run a multi-node cluster to develop and test !!  
As result you get system that is scale-minimized but similar to your production Redis Enterprise Software deployment.

***WARNING!*** This setup is not ideal for performance-sensitive production systems.  
Redis Cluster provides replication to protect against failures, but cluster cannot protect you against the failure of the **single host** so you can use it for test purpose only :)  
This deployment will look similar to the schema below:  

![RedisSchema](https://syngress.pl/images/redis_schema.png)

# Authorization
  
Access to the flight resource is possible by entering the ```user_id``` and ```authorization token``` in the request headers.  
We assume that each of the machines located at the airport has access to this application, each machine has its own ID, and unique token is generated based on the unique machine ID.  
Providing the appropriate ID pair corresponding to the string of characters submitted in token, allows access to flight resource.  
  
# Configuring and starting first node  

- Run ```docker-compopse up``` and wait until the image building process is over
- Go top https://localhost:8443
- Wait some time and allow Redis to initialize if you get HTTP 502
- Run setup and change Cluster Name to ```redis-cluster.desired-domain.com```, click next  

![RedisSetup1](https://syngress.pl/images/redis_setup_1.png)

- Skip cluster authentication field, we will be using free version  
- Enter your credentials and click next  

![RedisSetup2](https://syngress.pl/images/redis_setup_2.png)
![RedisSetup3](https://syngress.pl/images/redis_setup_3.png)  

Now system should reload page automatically, and you should see login page.  
Use credentials you have created in the above steps.  

# Add database  

- Select ```Redis Database``` and ```Single Region```  

![RedisSetup4](https://syngress.pl/images/redis_setup_4.png)

- Enter database name and memory limit  
- Click advanced options and set endpoint port number to ```12000```  
- Click ```activate```

![RedisSetup5](https://syngress.pl/images/redis_setup_5.png)

After few seconds database will be created and available for use.  
Now connect to database using ```redis-cli -p 12000``` from your local host cmd.  

Redis command line interface should start:

```code
127.0.0.1:12000>
```

You can test your database by add and list data  

- Add some key with new value ```SET mykey myvalue```
- Show value from selected key ```GET mykey```

If everything is ok and you can see ```myvalue``` response, we can enter inside container command line

- ```docker-compose exec redis-node1 bash```

Now we can check status of your deployment by enter ```rladmin status``` command

![RedisSetup6](https://syngress.pl/images/redis_setup_6.png)  

You can see IP address ```172.23.0.3``` of the container that will be use to create a multi-node cluster.  
Now we can add new nodes to our cluster and enable replication with sharding.  

# Add new nodes

We have running two more containers ```redis-node2``` and ```redis-node3```  
We can use them to add new nodes to our redis cluster.  
Because all nodes are running on the same ```redisnet``` network, we need to be sure that we use different endpoint ports for each container to avoid network conflicts.  
You can see ```docker-compose.yml``` for network endpoints configuration, look at ```ports:``` config sections.  
###
```
ports:
  - 8444:8443
  - 9444:9443
  - 12001:12000
```     

To configure two more nodes you need to use this URLs:  

- node 2: https://localhost:8444/
- node 3: https://localhost:8445/  

Now for 2 new nodes, do the following steps to add them to the cluster:  

- Click “Setup”
- In clusterconfiguration, select “Join Cluster”,
- Enter the IP address of the first node, in my case ```172.23.0.3``` into my environment
- Enter credentials you have used during the installation of the first node.
- Click “Next  

![RedisSetup7](https://syngress.pl/images/redis_setup_7.png)  

Your environment should be similar to:

![RedisSetup8](https://syngress.pl/images/redis_setup_8.png)  

When you use ```rladmin status``` command, you can see that your have only 1 shard in your cluster:

![ThreeNodes](https://syngress.pl/images/three_nodes.png)

# Clustering and Replication  

Shard is a horizontal partition of data in a database or search engine.  
Each individual partition is referred to as a shard or database shard.  
Each shard is held on a separate database server instance, to spread load.

![RedisShard](https://syngress.pl/images/redis_shard.jpg)

We have only 1 shard in our cluster.  
Let’s add new shard to our database.  
Login to any of the created nodes.  

- Click on the “databases” tab  
- Click on ```redis-db-1``` database  
- Click on the configuration  
- Click Edit  
- Check Replication checkbox, to create new shard that will be a replica, to provide high availability  
- Check “Database Clustering” and increase the number of shards to 2  

This will distribute the data in your database into 2 shards for better scalability.  
You can see that the UI indicated that you have 4 shards with replication.  
Your database that you have is divided in 2, and each of the portions of the database is replicated.  
Free version of Redis Enterprise is limited to 4 shards.  

- Click Update at the bottom of the page to save changes.  

Now go back to your redis command line and check again ```rladmin status```  

![RedisClusterShards](https://syngress.pl/images/redis_cluster_shards.png)  

You should have now 4 shards distributed on your 3 nodes.  
In the ```CLUSTER NODES``` section you can see that the ```*node:1``` is the master of your cluster.  
In the ```DATABASES``` section you can see that replication is enabled, and the database uses a ```dense``` placement.  
https://docs.redislabs.com/latest/rs/concepts/shard-placement-policy/  
In the ```SHARDS``` section you can see the various shards and their placement (node:1|2|3), their role (master|slave*) and their slots.  

# Application Architecture  

We are creating simple REST API using latest SpringBoot version.  
Application allows us to read data from selected public API, in our example data will be delivered from one of the international airport.  
Application architecture based on two data pipelines, first pipeline streams data form public API to KAFKA topic.  
Second pipeline consumes from topic and save data to ElasticSearch.  
From the same topic application read and save data to mongodb database, we use RedisCluster to store flight names.  
###
Application use DTO design pattern with custom error handler.  
DTO (Data Transfer objects) is a data container for moving simplifying data transfer between our application layers.  
We use some ValueObject that captures pieces of business logic and formatting rules, so it makes code more type safe and expressive.  
At the end we will automate everything with docker and StreamSets Data Collector.  

You should have some basic understanding of Docker and Data Engineering concepts such as transformations and streaming.  
The following diagram shows application architecture.  
  
![RedisClusterArchitecture](https://syngress.pl/images/redis_cluster_architecture.png) 
  
### StreamSets
  
StreamSets Data Collector is a lightweight and powerful engine that streams data in real time.  
To define the flow of data for Data Collector, you need to configure a pipeline.  
Pipeline consists of stages that represents the origin and destination of the pipeline and any additional processing that you want to perform.  
Stremsets comes bundled with many origin stage components to connect with almost all commonly used data sources !!  
  
In our example we have already configured data source from one of the International Airport.  
Our configured data source serve flights data information in Json format.  
Enter StreamSets GUI from ```http://localhost:18630/``` address with ```admin``` username and ```admin``` password, create new pipeline.  
  
Your pipeline may vary depending on the data structure returned by selected source, so we will focus only on explaining the individual steps.  
This is our ```Producer Pipeline``` which contains the source and the individual steps that define what is going to happen with your data.  
  
![StreamSetsProducerPipeline](https://syngress.pl/images/producer_pipeline.png)
  
- In the first step, we download data from source using an HTTP client
- In our example each JSON response is one big JSON object containing several different flights, so we use a Field Pivoter to flatten the Big JSON object into one message for each flights.
- Then we remove unnecessary fields from the JSON response
- We have some nested field in our JSON response, so we decide to flattens our JSON object in a more readable format
- Also we need to rename some fields
- At the end JSON in a structure suitable for us is send to Apache Kafka Producer
  
Pay attention to how much time you have to spent on writing code so your application can handle all of the above-mentioned steps.  
StreamSets does the trick with a few clicks from StreamSets GUI :)  
  
Now we need to checkout our stream setup.  
Clicking on validation button, application will check whether all steps have been configured correctly.  
Next thing is preview option that allows us to preview step by step what happen with our data.  
At each preview stage, we can go back or move forward one step to see input / output JSON.  
Also on each step of preview we can configure our pipeline stage and refresh to see our changes.  
  
Now we can check kafka whether the data goes correctly to the pipeline.  
We can use bootstrap consumer for that purpose  
Login to your kafka container shell  
  
```docker-compose exec kafka bash```  
  
Now run bootstrap consumer using our topic name  
  
```kafka-console-consumer --bootstrap-server kafka:9092 --topic flight_data```  
  
Run StreamSets Pipeline and wait for data, you should see data flow into the consumer.  
###
```aidl
{"flights":{"actualLandingTime":null,"estimatedLandingTime":null,"expectedTimeOnBelt":null,"flightDirection":"D","flightName":"KL1153","gate":"B20","id":"129069571587243645","route":{"departedFrom":"GOT"},"terminal":2}}
{"flights":{"actualLandingTime":null,"estimatedLandingTime":null,"expectedTimeOnBelt":null,"flightDirection":"D","flightName":"DL9660","gate":"B20","id":"129069570617252453","route":{"departedFrom":"GOT"},"terminal":2}}
{"flights":{"actualLandingTime":null,"estimatedLandingTime":null,"expectedTimeOnBelt":null,"flightDirection":"D","flightName":"EY7409","gate":"B20","id":"129069570804460039","route":{"departedFrom":"GOT"},"terminal":2}}
{"flights":{"actualLandingTime":null,"estimatedLandingTime":null,"expectedTimeOnBelt":null,"flightDirection":"D","flightName":"KQ1153","gate":"B20","id":"129069571605985255","route":{"departedFrom":"GOT"},"terminal":2}}
{"flights":{"actualLandingTime":null,"estimatedLandingTime":null,"expectedTimeOnBelt":null,"flightDirection":"D","flightName":"MF9323","gate":"B20","id":"129069571842945243","route":{"departedFrom":"GOT"},"terminal":2}}
{"flights":{"actualLandingTime":null,"estimatedLandingTime":null,"expectedTimeOnBelt":null,"flightDirection":"D","flightName":"MH5645","gate":"B20","id":"129069571850045029","route":{"departedFrom":"GOT"},"terminal":2}}
{"flights":{"actualLandingTime":null,"estimatedLandingTime":null,"expectedTimeOnBelt":null,"flightDirection":"D","flightName":"SU3306","gate":"B20","id":"129069572730689579","route":{"departedFrom":"GOT"},"terminal":2}}
{"flights":{"actualLandingTime":null,"estimatedLandingTime":null,"expectedTimeOnBelt":null,"flightDirection":"D","flightName":"KL1821","gate":"C10","id":"129069571587262585","route":{"departedFrom":"TXL"},"terminal":2}}
{"flights":{"actualLandingTime":null,"estimatedLandingTime":null,"expectedTimeOnBelt":null,"flightDirection":"D","flightName":"CZ7655","gate":"C10","id":"129069570530838371","route":{"departedFrom":"TXL"},"terminal":2}}
{"flights":{"actualLandingTime":null,"estimatedLandingTime":null,"expectedTimeOnBelt":null,"flightDirection":"D","flightName":"DL9570","gate":"C10","id":"129069570617249789","route":{"departedFrom":"TXL"},"terminal":2}}
```
  
# ElasticSearch and Kibana
  
Let's move to the next step, we will store all data in elasticsearch database.  
We use another StreamSets Pipeline to make that happen.  

![StreamSetsConsumerPipeline](https://syngress.pl/images/consumer_pipeline.png)  
  
All you need to do is create consumer and connect it directly to elasticsearch.  
Each StreamSets Pipeline can be started and stopped separately.  
Now run all StreamSets Pipelines and search your data through Kibana.  
  
Our elastic documentID is ```flightName``` and we set it that way ```${record:value('/flights/flightName')}```  
In our JSON response ```flightName``` attribute shows the name of the flight.  
We check EnableUpsert, so records will be automatically updated or inserted into ElasticSearch datadase, based on the DocumentID.  
If everything goes well, you should be able to get flights data through Kibana ```http://localhost:5601/```  
  
![ElasticStreamData](https://syngress.pl/images/elastic_stream_data.png)  

# Application Consumer

Our application will automatically connect to kafka topic with diffirent consumer groupID.  
Different consumer groupID allows us to download data from kafka topic, even if another consumers with different groupID took the same data from that topic.  
Consumer consume each message from topic and save that flight message as document to MongoDB collection.  
Our avro schema accept null values, this is related to source Airport data, which forwards messages with null values, when their system does not have complete flight related data.  
Null values will be automatically saved as empty string in mongo.  

![StreamSetsRecMongoDatabaseData](https://syngress.pl/images/rec_mongo_data.png)   
  
# Redis Cluster Data
  
Let's save some data to our "in memory" database cluster.  
In our specific example we use redis in non-cluster aware clients flavor.  
That mean we should use it with a regular client (it is like you are connecting to a single Redis process).  
With regular OSS cluster you need a cluster aware client, but for the purposes of this application that will not be implement.  
We are transferring flight data to redis from the kafka consumer class.  
We are store two information in redis database. Flight name and expected flight time on the belt.  
The above information will be displayed in the front application, on the arrival flight data panel.
      
# TABLE OF APPLICATION ENDPOINTS  
  
*USER*

| Method | Endpoint                                   |Note                      |
| ------ | ------------------------------------------ |------------------------- |
| POST   | v1/user/register                           | Create user object       |
| POST   | v1/user/login                              | User authorization       |
| GET    | v1/user/_id                                | Get user object          |  
  
*FLIGHT*  

| Method | Endpoint                                              |Note                      |
| ------ | ----------------------------------------------------- |------------------------- |
| POST   | v1/flight                                             | Create flight object     |
| GET    | v1/flight/:id                                         | Get flight object        |
| GET    | v1/flights?page=0&size=30&sort=flightId&direction=DESC| Get flight object        |
| PUT    | v1/flight/:id                                         | Update flight object     |
| DELETE | v1/flight/:id                                         | Delete flight object     |
  
###
```
[Request Headers]
{
    "Content-Type":"application/json"
    "Authorization":"user_token"
    "UserId":"5e6e9d3b835b3a1957152858"
}
```   
###  
```
[Get user request]
curl -X GET http://localhost:9000/v1/user/5e650b6a587c55098ac56c02 -H 'Content-Type: application/json' -H 'cache-control: no-cache' -H 'Authorization:jwt_token' -d

[Get user response]
{
    "id": "5e650b6a587c55098ac56c02",
    "username": "myusername",
    "email": "my_desired@email.com",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.test_token"
}
```  
  
# API Documentation

Check out our API documentations in a fast, organized, and searchable interface [Click Here](https://syngress.pl/apidoc/api_doc_redis_cluster.html)

**Free Software, Hell Yeah!**  
  
**THIS PROJECT IS STILL UNDER CONSTRUCTION**
