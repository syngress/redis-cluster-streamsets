package com.spring.rediscluster.service;

import com.spring.rediscluster.model.entity.user.User;
import com.spring.rediscluster.repository.UserRepository;
import com.spring.rediscluster.validator.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.bson.types.ObjectId;

@Service
public class UserServiceImpl implements UserService {
    private String salt = TokenService.getSalt(30);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenService tokenService;

    @Override
    @Transactional
    public User createUser(User user) {
        String securePassword = TokenService.generateSecurePassword(user.getPassword(), salt);
        user.setPassword(securePassword);
        user.setToken(TokenService.createToken(user.getId()));
        user.setSalt(salt);

        if (!validateUserPresence(user.getEmail())) {
            Validate.userValidator.userExist(false);
        } else {
            userRepository.save(user);

            return user;
        }

        return null;
    }

    @Override
    @Transactional
    public User getUser(ObjectId id, String header) {
        User userObject = userRepository.findById(id);
        validateUserObject(userObject);
        if (id.toHexString().equals(tokenService.getUserIdFromToken(header))) return userObject;
        else {
            Validate.userValidator.userUnauthorized(false);
        }

        return null;
    }

    @Override
    @Transactional
    public User loginUser(User user) {
        User userObject = userRepository.findByEmail(user.getEmail());
        validateUserObject(userObject);
        String formPassword = user.getPassword();
        String userPassword = userObject.getPassword();
        String userSalt = userObject.getSalt();

        boolean passwordMatch = TokenService.verifyUserPassword(formPassword, userPassword, userSalt);

        if (passwordMatch) return userObject;
        else {
            Validate.userValidator.userUnauthorized(false);
        }

        return null;
    }

    private void validateUserObject(final User userObject) {
        Validate.userValidator.userNotExist(userObject != null);
    }

    private boolean validateUserPresence(final String userEmail) {
        return userRepository.findByEmail(userEmail) == null;
    }

}
