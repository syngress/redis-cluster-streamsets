package com.spring.rediscluster.model.converter.flight;

import com.spring.rediscluster.model.entity.flight.Flight;
import com.spring.rediscluster.model.vo.flight.FlightVO;
import org.bson.types.ObjectId;

public class FlightConverter {

    private FlightConverter() {}

    public static Flight toFlight(FlightVO vo) {
        Flight flight = new Flight();
        flight.setId(new ObjectId());
        flight.setActualLandingTime(vo.actualLandingTime);
        flight.setEstimatedLandingTime(vo.estimatedLandingTime);
        flight.setExpectedTimeOnBelt(vo.expectedTimeOnBelt);
        flight.setFlightDirection(vo.flightDirection);
        flight.setFlightName(vo.flightName);
        flight.setGate(vo.gate);
        flight.setFlightId(vo.flightId);
        flight.setRoute(vo.route);
        flight.setTerminal(vo.terminal);

        return flight;
    }

    public static FlightVO toValueObject(Flight flight) {
        FlightVO vo = new FlightVO();
        vo.id = flight.getId();
        vo.actualLandingTime = flight.getActualLandingTime();
        vo.estimatedLandingTime = flight.getEstimatedLandingTime();
        vo.expectedTimeOnBelt = flight.getExpectedTimeOnBelt();
        vo.flightDirection = flight.getFlightDirection();
        vo.flightName = flight.getFlightName();
        vo.gate = flight.getGate();
        vo.flightId = flight.getFlightId();
        vo.route = flight.getRoute();
        vo.terminal = flight.getTerminal();

        return vo;
    }

}
