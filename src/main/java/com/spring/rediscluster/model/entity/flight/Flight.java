package com.spring.rediscluster.model.entity.flight;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.LinkedHashMap;
import java.util.Map;

@Document(collection = "#{@environment.getProperty('mongo.event.collection')}")
public class Flight {
    @Id
    private ObjectId id;
    private String actualLandingTime;
    private String estimatedLandingTime;
    private String expectedTimeOnBelt;
    private String flightDirection;
    private String flightName;
    private String gate;
    private String flightId;
    private Map<String, Object> route = new LinkedHashMap<>();
    private String terminal;

    public Flight() {}

    public Flight(
            ObjectId id,
            String actualLandingTime,
            String estimatedLandingTime,
            String expectedTimeOnBelt,
            String flightDirection,
            String flightName,
            String gate,
            String flightId,
            Map<String, Object> route,
            String terminal
    ) {
        this.id = id;
        this.actualLandingTime = actualLandingTime;
        this.estimatedLandingTime = estimatedLandingTime;
        this.expectedTimeOnBelt = expectedTimeOnBelt;
        this.flightDirection = flightDirection;
        this.flightName = flightName;
        this.gate = gate;
        this.flightId = flightId;
        this.route = route;
        this.terminal = terminal;
    }

    public String getId() {
        return id.toHexString();
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getActualLandingTime() {
        return actualLandingTime;
    }

    public void setActualLandingTime(String actualLandingTime) {
        this.actualLandingTime = actualLandingTime;
    }

    public String getEstimatedLandingTime() {
        return estimatedLandingTime;
    }

    public void setEstimatedLandingTime(String estimatedLandingTime) {
        this.estimatedLandingTime = estimatedLandingTime;
    }

    public String getExpectedTimeOnBelt() {
        return expectedTimeOnBelt;
    }

    public void setExpectedTimeOnBelt(String expectedTimeOnBelt) {
        this.expectedTimeOnBelt = expectedTimeOnBelt;
    }

    public String getFlightDirection() {
        return flightDirection;
    }

    public void setFlightDirection(String flightDirection) {
        this.flightDirection = flightDirection;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getGate() {
        return gate;
    }

    public void setGate(String gate) {
        this.gate = gate;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public Map<String, Object> getRoute() {
        return route;
    }

    @JsonAnySetter
    public void setRoute(Map<String, Object> route) {
        this.route = route;
    }

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

}
