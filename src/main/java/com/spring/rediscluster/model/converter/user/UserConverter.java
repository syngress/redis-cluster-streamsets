package com.spring.rediscluster.model.converter.user;

import com.spring.rediscluster.model.entity.user.User;
import com.spring.rediscluster.model.vo.user.UserVO;
import org.bson.types.ObjectId;

public class UserConverter {

    private UserConverter() {
    }

    public static User toUser(UserVO vo) {
        User user = new User();
        user.setId(new ObjectId());
        user.setUsername(vo.username);
        user.setEmail(vo.email);
        user.setPassword(vo.password);
        user.setToken(vo.token);
        user.setSalt(vo.salt);
        return user;
    }

    public static UserVO toValueObject(User user) {
        UserVO vo = new UserVO();
        vo.id = user.getId();
        vo.username = user.getUsername();
        vo.email = user.getEmail();
        vo.password = user.getPassword();
        vo.token = user.getToken();
        vo.salt = user.getSalt();
        return vo;
    }

}
