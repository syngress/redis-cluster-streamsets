package com.spring.rediscluster.validator;

import static com.spring.rediscluster.exception.ExceptionKey.unauthorized;
import static com.spring.rediscluster.validator.Validate.isTrue;

public class TokenValidator {
    public void isTokenValid(Boolean tokenIsValid) {
        isTrue(tokenIsValid, unauthorized, "Invalid token. The Token's Signature resulted invalid when verified");
    }

}
