package com.spring.rediscluster.model.converter.user;

import com.spring.rediscluster.model.entity.user.User;
import com.spring.rediscluster.model.vo.user.GetUserVO;
import org.bson.types.ObjectId;

public class GetUserConverter {

    private GetUserConverter() {
    }

    public static User toUser(GetUserVO vo) {
        User user = new User();
        user.setId(new ObjectId());
        user.setUsername(vo.username);
        user.setEmail(vo.email);
        user.setPassword(vo.password);
        user.setSalt(vo.salt);

        return user;
    }

    public static GetUserVO toValueObject(User user) {
        GetUserVO vo = new GetUserVO();
        vo.id = user.getId();
        vo.username = user.getUsername();
        vo.email = user.getEmail();
        vo.password = user.getPassword();
        vo.salt = user.getSalt();

        return vo;
    }

}
