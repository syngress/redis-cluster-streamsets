package com.spring.rediscluster.kafka.consumer;

import com.google.common.reflect.TypeToken;
import com.google.gson.*;
import com.spring.rediscluster.configuration.RedisConfiguration;
import com.spring.rediscluster.repository.FlightRepository;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import lombok.extern.apachecommons.CommonsLog;
import java.util.HashMap;
import java.util.Map;

import com.spring.rediscluster.GetFlightMessage;
import com.spring.rediscluster.model.entity.flight.Flight;

@Service
@CommonsLog(topic = "Consumer Log")
public class FlightConsumer {

    @Autowired
    FlightRepository flightRepository;

    private static final String TOPIC_NAME = "${topic.flight-messages}";
    private static final String GROUP_ID = "${spring.kafka.consumer.group-id}";

    @KafkaListener(topics = TOPIC_NAME, groupId = GROUP_ID)
    public void consume(ConsumerRecord<String, GetFlightMessage> record) {
        processMessage(record);
    }

    private void processMessage(final ConsumerRecord<String, GetFlightMessage> record) {
        String data = String.format("%s", record.value());
        JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
        JsonObject payload = jsonObject.getAsJsonObject("flights");

        try {
            storeFlightsTimeOnBelt(payload.get("flightName").toString(), payload.get("expectedTimeOnBelt").toString());

            ObjectId new_id = new ObjectId();
            JsonElement actualLandingTime = payload.get("actualLandingTime");
            JsonElement estimatedLandingTime = payload.get("estimatedLandingTime");
            JsonElement expectedTimeOnBelt = payload.get("expectedTimeOnBelt");
            JsonElement flightDirection = payload.get("flightDirection");
            JsonElement flightName = payload.get("flightName");
            JsonElement gate = payload.get("gate");
            JsonElement terminal = payload.get("terminal");
            JsonElement flightId = payload.get("flightId");

            Map<String, Object> route = new Gson().fromJson(
                    payload.getAsJsonObject("route"), new TypeToken<HashMap<String, Object>>() {}.getType()
            );

            Flight flightData = new Flight(
                    new_id,
                    !actualLandingTime.isJsonNull() ? actualLandingTime.getAsString() : "",
                    !estimatedLandingTime.isJsonNull() ? estimatedLandingTime.getAsString() : "",
                    !expectedTimeOnBelt.isJsonNull() ? expectedTimeOnBelt.getAsString() : "",
                    !flightDirection.isJsonNull() ? flightDirection.getAsString() : "",
                    !flightName.isJsonNull() ? flightName.getAsString() : "",
                    !gate.isJsonNull() ? gate.getAsString() : "",
                    !flightId.isJsonNull() ? flightId.getAsString() : "",
                    !route.isEmpty() ? route : null,
                    !terminal.isJsonNull() ? terminal.getAsString() : ""
            );

            flightRepository.save(flightData);
        } catch(Exception exception) {
            exception.printStackTrace();
        }
    }

    private void storeFlightsTimeOnBelt(String flightName, String expectedTimeOnBelt) {
        RedisConfiguration redisConfiguration = new RedisConfiguration();
        redisConfiguration.setKeyValue(flightName, expectedTimeOnBelt);
    }

}
