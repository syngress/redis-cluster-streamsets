package com.spring.rediscluster.database;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.spring.rediscluster.model.entity.flight.Flight;
import com.spring.rediscluster.repository.FlightRepository;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.List;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.data.mongodb.database=flightsTestDatabase"})
@AutoConfigureMockMvc
public class FlightDatabaseTest {

    @Autowired
    FlightRepository flightRepository;

    static final int quantity = 20;
    int pageSize = 30;
    int randomObject = ThreadLocalRandom.current().nextInt(1, 18 + 1);

    String json = "{ \"departedFrom\": \"PVG\" }";
    JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
    Map<String, Object> route = new Gson().fromJson(
            jsonObject, new TypeToken<HashMap<String, Object>>() {}.getType()
    );

    @Before
    public void init() {
        for (int i = 0; i < quantity; i++) {
            ObjectId new_id = new ObjectId();
            int dataWindow = (int) (Math.random() * 100);
            ObjectId newId = new ObjectId();

            Flight flight = new Flight(
                    newId,
                    "2020-03-28T04:43:00.000+01:00",
                    "2020-03-28T04:30:00.000+01:00",
                    "2020-03-28T05:19:03.000+01:00",
                    "A",
                    "MP8342",
                    "1",
                    "129167921012274080",
                    route,
                    "3"
            );

            flightRepository.save(flight);
        }
    }

    @Test
    public void databaseVerificationTest() {
        List<Flight> list = flightRepository.findAll();
        assertEquals(quantity, list.size());
    }

    @Test
    public void flightPagingTest() {
        Pageable pageable = PageRequest.of(0,pageSize);
        Page<Flight> flight = flightRepository.findAll(pageable);
        assertEquals(flight.getSize(),pageSize);
    }

    @Test
    public void flightPageAndSortTest() {
        Pageable pageable = PageRequest.of(0, pageSize , Sort.Direction.ASC ,"actualLandingTime");
        Page<Flight> flights = flightRepository.findAll(pageable);
        assertEquals(pageSize, flights.getSize());
        assertEquals("2020-03-28T04:43:00.000+01:00", flights.getContent().get(randomObject).getActualLandingTime());
        assertEquals("2020-03-28T04:30:00.000+01:00", flights.getContent().get(randomObject).getEstimatedLandingTime());
        assertEquals("2020-03-28T05:19:03.000+01:00", flights.getContent().get(randomObject).getExpectedTimeOnBelt());
        assertEquals("A", flights.getContent().get(randomObject).getFlightDirection());
        assertEquals("MP8342", flights.getContent().get(randomObject).getFlightName());
        assertEquals("1", flights.getContent().get(randomObject).getGate());
        assertEquals(route, flights.getContent().get(randomObject).getRoute());
        assertEquals("3", flights.getContent().get(randomObject).getTerminal());
    }

    @After
    public void deleteAllTest() {
        flightRepository.deleteAll();
    }

}
