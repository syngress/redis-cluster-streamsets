package com.spring.rediscluster.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.spring.rediscluster.configuration.GsonConfiguration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.spring.rediscluster.repository.UserRepository;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.data.mongodb.database=flightsTestDatabase"})
@AutoConfigureMockMvc
public class UserControllerTest {

    private MockMvc mockMvc;

    @Autowired
    UserRepository userRepository;

    @Autowired
    private UserController userController;

    @Autowired
    private ObjectMapper objectMapper;

    private MockHttpServletResponse createNewUserResponse;
    private String userData;

    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(userController)
                .build();

        userData = "{ \"username\": \"user_controller_user\", \"email\": \"user_controller@email.com\", \"password\": \"secret_password\" }";
        createNewUser();
    }

    public void createNewUser() throws Exception {
        try {
            ResultActions resultActions = (ResultActions) mockMvc.perform(
                    post("/v1/user/register")
                            .header("Content-Type", "application/json")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(GsonConfiguration.getGson().toJson(parseJsonObject(userData))))
                    .andDo(print())
                    .andExpect(status().isOk());

            MvcResult result = resultActions.andReturn();
            createNewUserResponse = result.getResponse();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @Order(1)
    public void loginUser() throws Exception {
        try {
            String testJson = createNewUserResponse.getContentAsString();
            JsonObject jsonObject = new JsonParser().parse(testJson).getAsJsonObject();

            this.mockMvc.perform(
                    post("/v1/user/login")
                            .header("Content-Type", "application/json")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(GsonConfiguration.getGson().toJson(parseJsonObject(userData))))
                    .andDo(print())
                    .andExpect(jsonPath("$.token").value(jsonObject.get("token").getAsString()))
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @Order(2)
    public void getUser() throws Exception {
        try {
            String testJson = createNewUserResponse.getContentAsString();
            JsonObject jsonObject = new JsonParser().parse(testJson).getAsJsonObject();

            mockMvc.perform(
                    get("/v1/user/"+jsonObject.get("id").getAsString())
                            .header("Content-Type", " application/json")
                            .header("Authorization", jsonObject.get("token").getAsString())
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andReturn();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void cleanupDatabase() throws Exception {
        try {
            userRepository.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JsonObject parseJsonObject(String data) throws Exception {
        try {
            JsonObject jsonData = new JsonParser().parse(data).getAsJsonObject();

            return jsonData;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
