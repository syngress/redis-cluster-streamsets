package com.spring.rediscluster.configuration;

import redis.clients.jedis.Jedis;

public class RedisConfiguration {

    Jedis jedis = new Jedis("172.18.0.2", 12000, false);

    public void main (String[] args) {
        try {
            jedis.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setKeyValue(String key, String value) {
        jedis.set(key, value);
    }

    public String getValue(String key) {
        return jedis.get(key);
    }

}
