package com.spring.rediscluster.model.vo.flight;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.data.mongodb.core.index.Indexed;
import java.util.Map;

public class FlightVO {
    @JsonProperty("id")
    @ApiModelProperty(required = true, value = "ObjectID")
    public String id;

    @JsonProperty("actualLandingTime")
    @ApiModelProperty(required = true, value = "ActualLendingTime")
    public String actualLandingTime;

    @JsonProperty("estimatedLandingTime")
    @ApiModelProperty(required = true, value = "EstimatedLandingTime")
    public String estimatedLandingTime;

    @JsonProperty("expectedTimeOnBelt")
    @ApiModelProperty(required = true, value = "ExpectedTimeOnBelt")
    public String expectedTimeOnBelt;

    @JsonProperty("flightDirection")
    @ApiModelProperty(required = true, value = "FlightDirection")
    public String flightDirection;

    @JsonProperty("flightName")
    @ApiModelProperty(required = true, value = "FlightName")
    public String flightName;

    @JsonProperty("gate")
    @ApiModelProperty(required = true, value = "Gate")
    public String gate;

    @JsonProperty("flightId")
    @Indexed(unique = true)
    @ApiModelProperty(required = true, value = "FlightId")
    public String flightId;

    @JsonProperty("route")
    @ApiModelProperty(required = true, value = "Route")
    public Map<String, Object> route;

    @JsonProperty("terminal")
    @ApiModelProperty(required = true, value = "Terminal")
    public String terminal;

}
