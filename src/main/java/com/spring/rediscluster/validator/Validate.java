package com.spring.rediscluster.validator;

import com.spring.rediscluster.controller.FlightController;
import com.spring.rediscluster.exception.CommonRestException;
import com.spring.rediscluster.exception.ExceptionKey;

import org.apache.commons.collections4.CollectionUtils;
import java.util.Collection;

public class Validate {

    public static final UserValidator userValidator = new UserValidator();
    public static final TokenValidator tokenValidator = new TokenValidator();
    public static final FlightValidator flightValidator = new FlightValidator();

    public static void isTrue(boolean condition, ExceptionKey exceptionKey, String description, Object... obj) {
        if (!condition) {
            throw new CommonRestException(exceptionKey.getHttpStatus(), exceptionKey.name(), String.format(description, obj));
        }
    }

    public static void isNotNull(Object value, ExceptionKey exceptionKey, String message, Object... values) {
        if (value == null) {
            throw new CommonRestException(exceptionKey.getHttpStatus(), exceptionKey.name(), String.format(message, values));
        }
    }

    public static void isNotEmpty(Collection<?> collection, ExceptionKey exceptionKey, String message, Object... values) {
        isTrue(CollectionUtils.isNotEmpty(collection), exceptionKey, message, values);
    }

}
