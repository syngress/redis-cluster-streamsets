package com.spring.rediscluster.exception;

public class CommonExceptionResponse {
    private String error;
    private String exception;
    private String time;

    public CommonExceptionResponse(String error, String exception, String time) {
        this.error = error;
        this.exception = exception;
        this.time = time;
    }

    public String getError() {
        return error;
    }

    public String getTime() {
        return time;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

}
