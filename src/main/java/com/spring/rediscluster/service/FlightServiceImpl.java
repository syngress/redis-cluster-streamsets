package com.spring.rediscluster.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.bson.types.ObjectId;

import com.spring.rediscluster.model.entity.flight.Flight;
import com.spring.rediscluster.repository.FlightRepository;
import com.spring.rediscluster.validator.Validate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@Service
public class FlightServiceImpl implements FlightService {

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private TokenService tokenService;

    @Override
    @Transactional
    public Flight createFlight(Flight flight, String authorization, String user_id) {
        authorizeRequest(authorization, user_id);
        flightRepository.save(flight);

        return flight;
    }

    @Override
    @Transactional
    public Flight getFlightById(ObjectId id, String authorization, String user_id) {
        authorizeRequest(authorization, user_id);
        Flight flightObject = flightRepository.findById(id);
        validateFlightObject(flightObject);

        return flightObject;
    }

    @Override
    @Transactional
    public Page<Flight> getAllFlights(int page, int size, String sort, String direction, String authorization, String user_id) {
        authorizeRequest(authorization, user_id);
        Sort.Direction sortDirection = direction.equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC;
        Pageable pages = PageRequest.of(page, size, sortDirection, sort);

        return flightRepository.findAll(pages);
    }

    @Override
    @Transactional
    public Flight modifyFlightById(ObjectId id, Flight flight, String authorization, String user_id) {
        authorizeRequest(authorization, user_id);
        flight.setId(id);
        flightRepository.save(flight);

        return flight;
    }

    @Override
    @Transactional
    public Flight deleteFlight(ObjectId id, String authorization, String user_id) {
        authorizeRequest(authorization, user_id);
        Flight flightObject = flightRepository.findById(id);
        validateFlightObject(flightObject);
        flightRepository.delete(flightObject);
        return flightObject;
    }

    private void validateFlightObject(final Flight flightObject) {
        Validate.flightValidator.flightNotExist(flightObject != null);
    }

    private boolean authorizeRequest(final String authorization, String user_id) {
        var objectId = new ObjectId(user_id);

        if (objectId.toHexString().equals(tokenService.getUserIdFromToken(authorization))) return true;
        else {
            Validate.userValidator.userUnauthorized(false);
        }

        return false;
    }

}
