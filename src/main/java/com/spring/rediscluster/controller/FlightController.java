package com.spring.rediscluster.controller;

import com.spring.rediscluster.api.FlightControllerApi;
import com.spring.rediscluster.model.vo.flight.FlightVO;
import com.spring.rediscluster.model.entity.flight.Flight;
import com.spring.rediscluster.model.converter.flight.FlightConverter;
import com.spring.rediscluster.service.FlightService;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Page;
import javax.validation.Valid;

@RestController
public class FlightController implements FlightControllerApi {

    @Autowired
    private FlightService service;

    public FlightVO createFlight(String authorization, String user_id, @RequestBody(required = true) @Valid FlightVO flightVO) {
        Flight flight = service.createFlight(FlightConverter.toFlight(flightVO), authorization, user_id);

        return FlightConverter.toValueObject(flight);
    }

    public FlightVO getFlightById(String authorization, String user_id, @PathVariable("id") ObjectId id) {
        Flight flight = service.getFlightById(id, authorization, user_id);

        return FlightConverter.toValueObject(flight);
    }

    public Page<FlightVO> getAllFlights(
            String authorization,
            String user_id,
            @RequestParam(value = "page", defaultValue = "0") int page,
            @RequestParam(value = "size", defaultValue = "30") int size,
            @RequestParam(value = "sort", defaultValue = "connectionDate", required = false) String sort,
            @RequestParam(value = "direction", defaultValue = "DESC", required = false) String direction) {
        final Page<Flight> connection = service.getAllFlights(page, size, sort, direction, authorization, user_id);

        return connection.map(FlightConverter::toValueObject);
    }

    public FlightVO modifyFlightById(String authorization, String user_id, @PathVariable("id") ObjectId id, FlightVO flightVO) {
        Flight flight = service.modifyFlightById(id, FlightConverter.toFlight(flightVO), authorization, user_id);

        return FlightConverter.toValueObject(flight);
    }

    public FlightVO deleteFlight(String authorization, String user_id, @PathVariable("id") ObjectId id) {
        Flight flight = service.deleteFlight(id, authorization, user_id);

        return FlightConverter.toValueObject(flight);
    }

}
