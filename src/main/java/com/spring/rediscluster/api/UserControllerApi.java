package com.spring.rediscluster.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;

import com.spring.rediscluster.model.ErrorResponse;
import com.spring.rediscluster.model.vo.user.*;

@Api(value = "v1")
public interface UserControllerApi {
    // Create User Object
    @ApiOperation(value = "", nickname = "createUser", notes = "Create New User",
            response = UserVO.class, tags = {"create"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = UserVO.class)})
    @PostMapping("/v1/user/register")
    UserVO createUser(@RequestBody(required = true) @Valid UserVO user);

    // Get User Object
    @ApiOperation(value = "", nickname = "getUser", notes = "Get User Object",
            response = GetUserVO.class, tags = {"get"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = UserVO.class),
            @ApiResponse(code = 400, message = "BadRequest", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class)})
    @GetMapping("/v1/user/{id}")
    GetUserVO getUser(@RequestHeader(value="Authorization") String headerStr, ObjectId id);

    // Login User
    @ApiOperation(value = "", nickname = "loginUser", notes = "Authorize User",
            response = UserVO.class, tags = {"login"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = UserVO.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class)})
    @PostMapping("/v1/user/login")
    UserVO loginUser(@RequestBody(required = true) @Valid UserVO user);

}
