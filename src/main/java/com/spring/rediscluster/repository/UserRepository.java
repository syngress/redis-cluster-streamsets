package com.spring.rediscluster.repository;

import com.spring.rediscluster.model.entity.user.User;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
    User findById(ObjectId id);
    User findByEmail(String email);

}
