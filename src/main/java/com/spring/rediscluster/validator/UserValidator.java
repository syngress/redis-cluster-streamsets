package com.spring.rediscluster.validator;

import static com.spring.rediscluster.exception.ExceptionKey.*;
import static com.spring.rediscluster.validator.Validate.isTrue;

public class UserValidator {
    public void userNotExist(Boolean userObject) {
        isTrue(userObject, notFound, "User not found");
    }

    public void userUnauthorized(Boolean userObject) {
        isTrue(userObject, unauthorized, "Unauthorized request");
    }

    public void userExist(Boolean userObject) {
        isTrue(userObject, conflict, "Resource already exists");
    }

}
