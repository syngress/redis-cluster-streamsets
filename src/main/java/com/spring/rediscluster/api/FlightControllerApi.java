package com.spring.rediscluster.api;

import com.spring.rediscluster.model.vo.flight.FlightVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.*;
import org.springframework.data.domain.Page;

import com.spring.rediscluster.model.ErrorResponse;

@Api(value = "v1")
public interface FlightControllerApi {
    // Create Flight Object
    @ApiOperation(value = "", nickname = "createFlight", notes = "Create New Flight",
            response = FlightVO.class, tags = {"create"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = FlightVO.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)})
    @PostMapping("/v1/flight")
    FlightVO createFlight(
            @RequestHeader(value="Authorization") String authorization,
            @RequestHeader(value="UserId") String user_id,
            @RequestBody(required = true) @Valid FlightVO flight
    );

    // Get Flight Object by ID
    @ApiOperation(value = "", nickname = "getFlightMessage", notes = "GetFlight",
            response = FlightVO.class, tags = {"get"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = FlightVO.class),
            @ApiResponse(code = 400, message = "BadRequest", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "NotFound", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)
    })
    @GetMapping("/v1/flight/{id}")
    FlightVO getFlightById(
            @RequestHeader(value="Authorization") String authorization,
            @RequestHeader(value="UserId") String user_id,
            ObjectId id
    );

    // Get Flight Collection Pageable
    @ApiOperation(value = "", nickname = "getAllFlights", notes = "GetFlightCollection",
            response = FlightVO.class, tags = {"get"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = FlightVO.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)})
    @GetMapping("/v1/flights")
    Page<FlightVO> getAllFlights(
            @RequestHeader(value="Authorization") String authorization,
            @RequestHeader(value="UserId") String user_id,
            int page,
            int size,
            String sort,
            String direction
    );

    // Edit Flight Object
    @ApiOperation(value = "", nickname = "editFlight", notes = "EditFlightObject",
            response = FlightVO.class, tags = {"get"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = FlightVO.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)})
    @PutMapping("/v1/flight/{id}")
    FlightVO modifyFlightById(
            @RequestHeader(value="Authorization") String authorization,
            @RequestHeader(value="UserId") String user_id,
            ObjectId id,
            @RequestBody FlightVO flight
    );

    // Delete Flight Object
    @ApiOperation(value = "", nickname = "deleteFlight", notes = "DeleteFlightObject",
            response = FlightVO.class, tags = {"delete"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = FlightVO.class),
            @ApiResponse(code = 500, message = "ServerError", response = ErrorResponse.class)})
    @DeleteMapping("/v1/flight/{id}")
    FlightVO deleteFlight(
            @RequestHeader(value="Authorization") String authorization,
            @RequestHeader(value="UserId") String user_id,
            ObjectId id
    );

}
