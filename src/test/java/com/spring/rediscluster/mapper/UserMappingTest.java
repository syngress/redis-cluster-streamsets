package com.spring.rediscluster.mapper;

import com.spring.rediscluster.model.entity.user.User;
import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.bson.types.ObjectId;
import static org.junit.Assert.assertEquals;

public class UserMappingTest {
    private static final ModelMapper modelMapper = new ModelMapper();

    @Test
    public void testUserModel() {
        ObjectId newId = new ObjectId();
        User user = new User();

        user.setId(newId);
        user.setUsername("TestUser");
        user.setEmail("test@email.com");
        user.setPassword("some_super_secret_password");
        user.setToken("test_token");
        user.setSalt("salt");

        User mapped = modelMapper.map(user, User.class);

        assertEquals(newId.toHexString(), mapped.getId());
        assertEquals(user.getUsername(), mapped.getUsername());
        assertEquals(user.getEmail(), mapped.getEmail());
        assertEquals(user.getPassword(), mapped.getPassword());
        assertEquals(user.getToken(), mapped.getToken());
        assertEquals(user.getSalt(), mapped.getSalt());
    }

}
