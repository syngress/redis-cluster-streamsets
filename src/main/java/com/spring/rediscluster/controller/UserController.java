package com.spring.rediscluster.controller;

import com.spring.rediscluster.api.UserControllerApi;
import com.spring.rediscluster.model.converter.user.GetUserConverter;
import com.spring.rediscluster.model.converter.user.UserConverter;
import com.spring.rediscluster.model.entity.user.User;
import com.spring.rediscluster.model.vo.user.GetUserVO;
import com.spring.rediscluster.model.vo.user.UserVO;
import com.spring.rediscluster.service.UserService;
import javax.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController implements UserControllerApi {

    @Autowired
    private UserService service;

    public UserVO createUser(@RequestBody(required = true) @Valid UserVO userVO) {
        User user = service.createUser(UserConverter.toUser(userVO));

        return UserConverter.toValueObject(user);
    }

    public GetUserVO getUser(String header, @PathVariable("id") ObjectId id) {
        User user = service.getUser(id, header);

        return GetUserConverter.toValueObject(user);
    }

    public UserVO loginUser(@RequestBody(required = true) @Valid UserVO userVO) {
        User user = service.loginUser(UserConverter.toUser(userVO));

        return UserConverter.toValueObject(user);
    }

}
