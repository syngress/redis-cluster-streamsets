package com.spring.rediscluster.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.spring.rediscluster.model.entity.flight.Flight;

public interface FlightRepository extends MongoRepository<Flight, String> {
    Flight findById(ObjectId id);

}
