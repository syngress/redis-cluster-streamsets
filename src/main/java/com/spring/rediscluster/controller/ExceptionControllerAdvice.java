package com.spring.rediscluster.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import java.time.LocalDateTime;

import com.spring.rediscluster.exception.CommonExceptionResponse;
import com.spring.rediscluster.exception.CommonRestException;

@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler {

    LocalDateTime today = LocalDateTime.now();
    String timeDateNow = today.toString();

    @ExceptionHandler(value = CommonRestException.class)
    public ResponseEntity<CommonExceptionResponse> handleRestException(final CommonRestException cre) {
        CommonExceptionResponse commonExceptionResponse = new CommonExceptionResponse(cre.getMessage(), cre.getDescription(), timeDateNow);

        return new ResponseEntity<>(commonExceptionResponse, cre.getHttpStatus());
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<CommonExceptionResponse> handleOtherException(final Exception e) {
        String message = e.getMessage() == null ? "Unexpected error, incorrect data provided" : e.getMessage();
        CommonExceptionResponse commonExceptionResponse = new CommonExceptionResponse("Other exception", message, timeDateNow);

        return new ResponseEntity<>(commonExceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}