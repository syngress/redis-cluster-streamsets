package com.spring.rediscluster.mapper;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.spring.rediscluster.model.entity.flight.Flight;

import org.junit.Test;
import org.modelmapper.ModelMapper;
import org.bson.types.ObjectId;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class FlightMappingTest {
    private static final ModelMapper modelMapper = new ModelMapper();

    @Test
    public void testFlightModel() {
        ObjectId newId = new ObjectId();
        String json = "{ \"departedFrom\": \"PVG\" }";
        JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
        Map<String, Object> route = new Gson().fromJson(
                jsonObject, new TypeToken<HashMap<String, Object>>() {}.getType()
        );

        Flight flight = new Flight();

        flight.setId(newId);
        flight.setActualLandingTime("2020-03-28T04:43:00.000+01:00");
        flight.setEstimatedLandingTime("2020-03-28T04:30:00.000+01:00");
        flight.setExpectedTimeOnBelt("2020-03-28T05:19:03.000+01:00");
        flight.setFlightDirection("A");
        flight.setFlightName("MP8342");
        flight.setGate("1");
        flight.setFlightId("129167921012274080");
        flight.setRoute(route);
        flight.setTerminal("3");

        Flight mapped = modelMapper.map(flight, Flight.class);

        assertEquals(newId.toHexString(), mapped.getId());
        assertEquals(flight.getActualLandingTime(), mapped.getActualLandingTime());
        assertEquals(flight.getEstimatedLandingTime(), mapped.getEstimatedLandingTime());
        assertEquals(flight.getExpectedTimeOnBelt(), mapped.getExpectedTimeOnBelt());
        assertEquals(flight.getFlightDirection(), mapped.getFlightDirection());
        assertEquals(flight.getFlightName(), mapped.getFlightName());
        assertEquals(flight.getGate(), mapped.getGate());
        assertEquals(flight.getFlightId(), mapped.getFlightId());
        assertEquals(flight.getRoute(), mapped.getRoute());
        assertEquals(flight.getTerminal(), mapped.getTerminal());
    }
    
}
