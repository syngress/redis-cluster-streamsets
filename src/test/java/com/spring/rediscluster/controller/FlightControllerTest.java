package com.spring.rediscluster.controller;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.spring.rediscluster.model.entity.flight.Flight;
import com.spring.rediscluster.repository.FlightRepository;
import com.spring.rediscluster.configuration.GsonConfiguration;
import com.spring.rediscluster.repository.UserRepository;
import org.bson.types.ObjectId;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Order;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"spring.data.mongodb.database=flightsTestDatabase"})
@AutoConfigureMockMvc
public class FlightControllerTest {

    private MockMvc mockMvc;
    
    @Autowired
    private FlightController flightController;

    @Autowired
    private UserController userController;

    @Autowired
    FlightRepository flightRepository;

    @Autowired
    UserRepository userRepository;

    String json = "{ \"username\": \"flight_controller_user\", \"email\": \"flight_controller@email.com\", \"password\": \"secret_password\" }";
    JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();

    private MockHttpServletResponse contentAsString;

    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders
                .standaloneSetup(flightController, userController)
                .build();

        createNewUser();
    }

    public void createNewUser() throws Exception {
        try {
            ResultActions resultActions = (ResultActions) mockMvc.perform(
                    post("/v1/user/register")
                            .header("Content-Type" , "application/json")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(GsonConfiguration.getGson().toJson(jsonObject)))
                    .andDo(print())
                    .andExpect(status().isOk());

            MvcResult result = resultActions.andReturn();
            contentAsString = result.getResponse();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @Order(1)
    public void createNewFlightTest() throws Exception {
        try {
            String testJson = contentAsString.getContentAsString();
            JsonObject userData = new JsonParser().parse(testJson).getAsJsonObject();

            ObjectId newId = new ObjectId();
            String json = "{ \"departedFrom\": \"PVG\" }";
            JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
            Map<String, Object> route = new Gson().fromJson(
                    jsonObject, new TypeToken<HashMap<String, Object>>() {}.getType()
            );

            Flight flight = new Flight(
                    newId,
                    "2020-03-28T04:43:00.000+01:00",
                    "2020-03-28T04:30:00.000+01:00",
                    "2020-03-28T05:19:03.000+01:00",
                    "A",
                    "MP8342",
                    "1",
                    "129167921012274080",
                    route,
                    "3"
                    );

            mockMvc.perform(
                    post("/v1/flight")
                            .header("Content-Type" , "application/json")
                            .header("Authorization" , userData.get("token").getAsString())
                            .header("UserId" , userData.get("id").getAsString())
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(GsonConfiguration.getGson().toJson(flight)))
                    .andDo(print())
                    .andExpect(status().isOk());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    @Order(2)
    public void getAllFlights() throws Exception {
        try {
            String testJson = contentAsString.getContentAsString();
            JsonObject userData = new JsonParser().parse(testJson).getAsJsonObject();

            mockMvc.perform(
                    get("/v1/flights")
                            .header("Authorization" , userData.get("token").getAsString())
                            .header("UserId" , userData.get("id").getAsString())
                            .contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andReturn();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @After
    public void cleanupDatabases() throws Exception {
        try {
            flightRepository.deleteAll();
            userRepository.deleteAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
