package com.spring.rediscluster.service;

import com.spring.rediscluster.model.entity.user.User;
import org.bson.types.ObjectId;

public interface UserService {
    User createUser(User user);
    User getUser(ObjectId id, String header);
    User loginUser(User user);

}
